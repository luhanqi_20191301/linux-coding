#include <stdio.h>
#include <unistd.h>
#include <signal.h>
void handler(int sig,siginfo_t *siginfo,void *context){
	printf("handel: sig=%d from pid=%d uid=%d\n",sig,siginfo->si_pid,siginfo->si_uid);
}
int main(int argc,char*argv[]){
	struct sigaction act;
	memset(&act,0,sizeof(act));
	act.sa_sigaction=&handler;
	act.sa_flags=SA_SIGINFO;
	sigaction(SIGTERM,&act,NULL);
        printf("proc pid=%d looping\n",getpid());
	while(1){
		sleep(10);
	}
}
