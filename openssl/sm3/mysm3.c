#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
void tDigest()
{
    unsigned char sm3_value[EVP_MAX_MD_SIZE];   //保存输出的摘要值的数组
    int sm3_len, i;
    EVP_MD_CTX *sm3ctx;                         //EVP消息摘要结构体
    sm3ctx = EVP_MD_CTX_new();//调用函数初始化
   // char msg1[] = "201912012019120220191203201912042019120520191206201912072019120820191209201912102019121120191212201912132019121420191215201912162019121720191218201912192019122020191221201912222019122320191224201912252019122620191227201912282019122920191230201912312019123220191301201913022019130320191304201913052019130620191307201913082019130920191310201913112019131220191313201913142019131520191316201913172019131820191319201913202019132120191322201913232019132420191325201913262019132720191328201913292019133020191331";              //待计算摘要的消息1
    char msg1[] = "20191301";              //待计算摘要的消息2
     
    EVP_MD_CTX_init(sm3ctx);                    //初始化摘要结构体
    EVP_DigestInit_ex(sm3ctx, EVP_sm3(), NULL); //设置摘要算法和密码算法引擎，这里密码算法使用sm3，算法引擎使用OpenSSL默认引擎即软算法
    EVP_DigestUpdate(sm3ctx, msg1, strlen(msg1));//调用摘要UpDate计算msg1的摘要
    //EVP_DigestUpdate(sm3ctx, msg2, strlen(msg2));//调用摘要UpDate计算msg2的摘要 
    EVP_DigestFinal_ex(sm3ctx, sm3_value, &sm3_len);//摘要结束，输出摘要值   
    EVP_MD_CTX_reset(sm3ctx);                       //释放内存
    printf("原始数据%s摘要值为:\n",msg1); 
   // printf("原始数据%s和%s的摘要值为:\n",msg1,msg2);
    for(i = 0; i < sm3_len; i++)
    {
        printf("0x%02x ", sm3_value[i]);
    }
    printf("\n");
}
int main()
{
    OpenSSL_add_all_algorithms();
    tDigest();
    return 0;
}

