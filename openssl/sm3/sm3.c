#include <stdio.h>
#include <string.h>
#include "openssl/evp.h"
#include "err.h"

FILE *fp;

void tDigest(FILE *file){
	unsigned char md_value[EVP_MAX_MD_SIZE];
	int md_len, i;
	EVP_MD_CTX *mdctx;
	char msg1[10000];
  	fscanf(fp, "%s", msg1);
	mdctx = EVP_MD_CTX_new();
	EVP_MD_CTX_init(mdctx);

	EVP_DigestInit_ex(mdctx, EVP_sm3(), NULL);
	EVP_DigestUpdate(mdctx, msg1, strlen(msg1));
	EVP_DigestFinal_ex(mdctx, md_value, &md_len);
	EVP_MD_CTX_free(mdctx);

//	printf("原始数据%s的摘要值为：\n", msg1);
	for(i = 0; i < md_len; i++){
		printf("%02x", md_value[i]);
	}
	printf("\n");
}

int main(int argc, char *argv[]){
	OpenSSL_add_all_algorithms();
	fp = fopen(argv[1], "r");
	tDigest(fp);
	fclose(fp);
	return 0;
}

