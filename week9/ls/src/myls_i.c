#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>

void show_stat_info(char *filename, struct stat *buf)
{
    printf("mode\t\t:%o\n", buf->st_mode);
    printf("linkers\t\t:%d\n",(int)buf->st_nlink);
    printf("user\t\t:%d\n", buf->st_uid);
    printf("group_id\t:%d\n", buf->st_gid);
    printf("size\t\t:%d\n", (int)buf->st_size);
    printf("modtime\t\t:%d\n", (int)buf->st_mtime);
    printf("group_name\t:%s\n", filename);
}

int main(int argc, char **argv)
{
    struct stat info;

    if (argc > 1)
    {
        if (stat(argv[1], &info) != -1)
        {
            show_stat_info(argv[1], &info);
        }
        else
        {
            perror(argv[1]);
        }
    }

    return 0;
}
