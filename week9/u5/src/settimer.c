#include <signal.h>
#include <stdio.h>
#include<sys/time.h>
int count=0;
void timer_hanler(int sig){
	struct itimerval t;
	printf("timer_hanler:signal=%d count=%d\n",sig ,++count);
	if(count>=8){
		printf("cancel timer\n");
		t.it_value.tv_sec=0;
		t.it_value.tv_usec=0;
		setitimer(ITIMER_VIRTUAL,&t,NULL);
	}
}
int main(){
	struct itimerval timer;
	signal(SIGVTALRM,timer_hanler);
	timer.it_value.tv_sec=0;
	timer.it_value.tv_usec=100000;
	timer.it_interval.tv_sec=1;
	timer.it_interval.tv_usec=0;
	setitimer(ITIMER_VIRTUAL,&timer,NULL);
	while(1);
}
