#ifndef _UTIL_H_
#define _UTIL_H_

int Hex2Char(int fromi,char * toc);
int Char2Hex(char fromc,int * toi);

int Bitstr2ByteArr(char * bs,char * ba);
int ByteArr2Bitstr(char * ba,char * bs);

#endif