#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <unistd.h>
#include <asm/ioctls.h>

#define MYPORT  8887
#define BUFFER_SIZE 1024
 
int main()
{
    ///定义sockfd
    int sock_cli = socket(AF_INET,SOCK_STREAM, 0);
 
    ///定义sockaddr_in
    struct sockaddr_in servaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(MYPORT);  ///服务器端口
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");  ///服务器ip
 
    ///连接服务器，成功返回0，错误返回-1
/*    if (connect(sock_cli, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        perror("connect");
        exit(1);
    }*/
     //设socket为非阻塞
    unsigned long ul=1;
    int rm=ioctl(sock_cli ,FIONBIO,&ul);
    if(rm==-1)
    {
      close(sock_cli);
      return 0;  
    }

    if (connect(sock_cli, (struct sockaddr *)&servaddr, sizeof(servaddr)) == 0)
    {
     printf("connected/n");//正常连接(小小：使用gdb调试时，从来走不到这一步"设置为非阻塞，connect调用后，无论连接是否建立立即返回-1")
    }
    if(errno!=EINPROGRESS)//若errno不是EINPROGRESS，则出错(EINPROGRESS:以非阻塞的方式来进行连接的时候，返回的结果如果是 -1,这并不代表这次连接发生了错误，如果它的返回结果是 EINPROGRESS，那么就代表连接还在进行中)
    {
     perror("connect");
     printf("cannot connect");
     return 0;
    }    
    //使用select设置超时
    struct timeval timeout={0,3};
    fd_set r;         
    FD_ZERO(&r);
    FD_SET(sock_cli,&r);
    timeout.tv_sec=0;   
    timeout.tv_usec=100;
    int retval = select(sock_cli+1,NULL,&r,NULL,&timeout);
    if(retval==-1)
    {
      perror("select");
      return 0;
    }
    else if(retval == 0)
    {
      fprintf(stderr,"Timeout/n");
      return 0;
    }
    printf("connected\n");
    //将socket设置回正常的阻塞模式
    unsigned long  ul1=0;
    rm=ioctl(sock_cli,FIONBIO,(unsigned long*)&ul1);
    if(rm==-1)
    {
      close(sock_cli);
      return 0;     
    }
   
 //   struct timeval timeout={3,0};//3s
    int ret=setsockopt(sock_cli,SOL_SOCKET,SO_SNDTIMEO,(const char*)&timeout,sizeof(timeout));
char sendbuf[BUFFER_SIZE];
    char recvbuf[BUFFER_SIZE];

   // 如果ret==0 则为成功,-1为失败,这时可以查看errno来判断失败原因
    int recvd=recv(sock_cli,recvbuf,1024,0);
    if(recvd==-1&&errno==EAGAIN)
   {
        printf("timeout\n");
   }
//    char sendbuf[BUFFER_SIZE];
  //  char recvbuf[BUFFER_SIZE];
    while (fgets(sendbuf, sizeof(sendbuf), stdin) != NULL)
    {
        send(sock_cli, sendbuf, strlen(sendbuf),0); ///发送
        if(strcmp(sendbuf,"exit\n")==0)
            break;
        recv(sock_cli, recvbuf, sizeof(recvbuf),0); ///接收
        fputs(recvbuf, stdout);
 
        memset(sendbuf, 0, sizeof(sendbuf));
        memset(recvbuf, 0, sizeof(recvbuf));
    }
 
    close(sock_cli);
    return 0;
}
